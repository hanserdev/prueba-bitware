-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-07-05 06:55:05.888

-- tables
-- Table: Cajeros
CREATE TABLE Cajeros (
    cajero int NOT NULL,
    NomApels varchar(255) NOT NULL,
    CONSTRAINT Cajeros_pk PRIMARY KEY (cajero)
);

-- Table: Maquinas_Registradoras
CREATE TABLE Maquinas_Registradoras (
    maquina int NOT NULL,
    piso int NOT NULL,
    CONSTRAINT Maquinas_Registradoras_pk PRIMARY KEY (maquina)
);

-- Table: Productos
CREATE TABLE Productos (
    producto int NOT NULL,
    nombre varchar(100) NOT NULL,
    precio varchar(100) NOT NULL,
    CONSTRAINT Productos_pk PRIMARY KEY (producto)
);

-- Table: Venta
CREATE TABLE Venta (
    cajero int NOT NULL,
    producto int NOT NULL,
    Registradoras_maquina int NOT NULL
);

-- foreign keys
-- Reference: Venta_Cajeros (table: Venta)
ALTER TABLE Venta ADD CONSTRAINT Venta_Cajeros FOREIGN KEY Venta_Cajeros (cajero)
    REFERENCES Cajeros (cajero);

-- Reference: Venta_Maquinas_Registradoras (table: Venta)
ALTER TABLE Venta ADD CONSTRAINT Venta_Maquinas_Registradoras FOREIGN KEY Venta_Maquinas_Registradoras (Registradoras_maquina)
    REFERENCES Maquinas_Registradoras (maquina);

-- Reference: Venta_Productos (table: Venta)
ALTER TABLE Venta ADD CONSTRAINT Venta_Productos FOREIGN KEY Venta_Productos (producto)
    REFERENCES Productos (producto);

-- End of file.

