var model             = require('../models/rest')
var request           = require('request')

module.exports = {
  getData : getData,
  postData : postData,
  putData : putData
}

function getData(d) {
  return new Promise( async function(resolve, reject) {
    request.get({uri: 'http://187.188.122.85:8091/NutriNET/Cliente'}, function(err, result, body){
      if (err) {
        resolve({err:true, descripcion: err})
      }else {
        resolve({err: false, data: body})
      }
    })
  })
}

function postData(d) {
  console.log(d, '***********');
  return new Promise( async function(resolve, reject) {
    request({
      uri: 'http://187.188.122.85:8091/NutriNET/Cliente',
      method: "POST",
      json: d
    }, function(err, result, body){
      if (err) {
        resolve({err:true, descripcion: err})
      }else {
        resolve({err: false, data: body})
      }
    })
  })
}

function putData(d) {
  console.log(d, '***********');
  return new Promise( async function(resolve, reject) {
    request({
      uri: `http://187.188.122.85:8091/NutriNET/Cliente/${d.Cliente}`,
      method: "PUT",
      json: d
    }, function(err, result, body){
      if (err) {
        resolve({err:true, descripcion: err})
      }else {
        resolve({err: false, data: body})
      }
    })
  })
}
