var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.redirect('imc')
});

router.get('/imc', function(req, res) {
  res.render('imc', {usuario:true, url : `https://hanserdev.site/bitware/`})
});

router.get('/rest', function(req, res) {
  res.render('rest', {usuario:true, url : `https://hanserdev.site/bitware/`})
});

router.get('/sql', function(req, res) {
  res.render('sql', {usuario:true, url : `https://hanserdev.site/bitware/`})
});


module.exports = router;
