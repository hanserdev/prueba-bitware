var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/rest');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');

router.get('/get-data', getData)
router.post('/post-data', postData)
router.put('/put-data', putData)

function getData(req, res) {
  ctrl.getData().then(function(result){
    res.json(result)
  })
}

function postData(req, res) {
  var d = req.body
  ctrl.postData(d).then(function(result){
    res.json(result)
  })
}

function putData(req, res) {
  var d = req.body
  ctrl.putData(d).then(function(result){
    res.json(result)
  })
}

module.exports = router;
