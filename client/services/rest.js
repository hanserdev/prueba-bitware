// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('restService', ['$http', function($http) {
  var url = "https://hanserdev.site/bitware";
  var urlBase = url + '/rest';

  this.getData = function() {
    return $http.get(urlBase + '/get-data')
  }

  this.postData = function(d) {
    return $http.post(urlBase + '/post-data', d)
  }

  this.putData = function(d) {
    return $http.put(urlBase + '/put-data', d)
  }

}]);
