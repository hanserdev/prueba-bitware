require('../services/imc');
var alertify = require('alertifyjs')


angular.module(MODULE_NAME)
.controller('imcCtrl', ['$scope', 'imcService', '$timeout', function($scope, imcService, $timeout) {
  var ctrl = this;
  $scope.init = init;
  $scope.calcularIMC = calcularIMC;

  $scope.persona = {};

  function init() {
    console.log('Archivo build.js iniciado correctamente');
  }

  function between(imc, min, max) {
    return imc >= min && imc <= max;
  }

  function calcularIMC() {
    var persona = $scope.persona
    console.log('Datos de la persona:', persona);
    if (persona.edad >= 18) {
      console.log('Edad:', persona.edad, 'La persona es mayor de edad');
      $scope.persona.validarEdad = 'La persona es mayor de edad'
    }else {
      console.log('Edad:', persona.edad, 'La persona es menor de edad');
      $scope.persona.validarEdad = 'La persona es menor de edad'
    }
    if (persona.nombre === undefined || persona.edad === undefined || persona.sexo === undefined || persona.peso === undefined || persona.altura === undefined ) {
      alertify.error('Todos los campos son requeridos');
    }else if (persona.sexo === 'h') {
      var imc = persona.peso/((persona.altura/100)*(persona.altura/100));
      if (between(imc, 20.000, 25.999)) {
        console.log('peso normal', 0);
        $scope.persona.status = 'Peso normal'
      }else if (imc <= 19.999) {
        console.log('falta de peso', -1);
        $scope.persona.status = 'Falta de peso'
      }else if (imc >= 26.000) {
        console.log('sobrepeso', 1);
        $scope.persona.status = 'Sobrepeso'
      }
      $scope.datosShow = true
    }else {
      var imc = persona.peso/((persona.altura/100)*(persona.altura/100));
      if (between(imc, 19.000, 24.999)) {
        console.log('peso normal', 0);
        $scope.persona.status = 'Peso normal'
      }else if (imc <= 18.999) {
        console.log('falta de peso', -1);
        $scope.persona.status = 'Falta de peso'
      }else if (imc >= 25.000) {
        console.log('sobrepeso', 1);
        $scope.persona.status = 'Sobrepeso'
      }
      $scope.datosShow = true
    }
    var caracteres = "A1B2C3D4E5F6G7H8I9J0K1M2N3P4Q5R6S7T8U9V0W";
    var nss = "";
    for (i=0; i<8; i++) nss += caracteres.charAt(Math.floor(Math.random()*8));
    $scope.persona.imc = imc.toFixed(2);
    $scope.persona.nss = nss
    console.log('IMC:', imc, 'NSS:', nss);
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
