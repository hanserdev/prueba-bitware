require('../services/rest');
var alertify = require('alertifyjs')


angular.module(MODULE_NAME)
.controller('restCtrl', ['$scope', 'restService', '$timeout', function($scope, restService, $timeout) {
  var ctrl = this;
  $scope.init = init;
  $scope.getData = getData;
  $scope.postData = postData;
  $scope.putData = putData;

  function init() {
    console.log('Archivo build.js iniciado correctamente');
  }

  function getData() {
    restService.getData()
    .success(function(res){
      console.log('Respuesta del servidor:', JSON.parse(res.data));
      $scope.datos = JSON.parse(res.data);
      $scope.tablaShow = true
    })
  }

  function postData() {
    var d = {
      Nombre: "JOSE",
      Apellidos: "HERNADEZ",
      Nombre_Usuario: "HANSER",
      Correo_Electronico: "hanser@hotmail.com",
      Contraseña: "hanser9" }
    restService.postData(d)
    .success(function(res){
      alert(res.data[0].Mensaje)
    })
  }

  function putData() {
    var d = {
      Cliente: 59,
      Edad: 23,
      Estatura: 1.79,
      Peso: 83,
      GB: 2000,
    }
    restService.putData(d)
    .success(function(res){
      alert(res.data[0].Mensaje)
    })
  }


}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
