require('../services/rest');
var alertify = require('alertifyjs')


angular.module(MODULE_NAME)
.controller('sqlCtrl', ['$scope', 'restService', '$timeout', function($scope, restService, $timeout) {
  var ctrl = this;
  $scope.init = init;
  $scope.modal = modal;


  function init() {
    console.log('Archivo build.js iniciado correctamente');

  }

  function modal(i) {
    if (i === 1) {
      $('#m1').modal('show');
    }else if (i === 2) {
      $('#m2').modal('show');
    }else if (i === 3) {
      $('#m3').modal('show');
    }else if (i === 4) {
      $('#m4').modal('show');
    }else if (i === 5) {
      $('#m5').modal('show');
    }else if (i === 6) {
      $('#m6').modal('show');
    }
    console.log(i);
  }


}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
